# catkin workspace with packages manager

## Usage

First of all, you will need 'php' and some libraries: make sure to install
necessary packages:

    sudo apt-get install php php-xml

First, you'll need to install `catkin`, the most convenient method is the following

    sudo apt-get install python-pip python-empy
    sudo pip install -U catkin_tools


Then, clone this repository and enter it:

    git clone https://bitbucket.org/pfe_unit_tests/workspace.git
    cd workspace

Run the setup:

    ./workspace setup

Install the required repositories
    ./workspace install git@bitbucket.org:pfe_unit_tests/robocup_referee.git
    ./workspace install git@bitbucket.org:pfe_unit_tests/graphs.git
    ./workspace install git@bitbucket.org:pfe_unit_tests/csa_mdp_experiments.git
    ./workspace install git@bitbucket.org:pfe_unit_tests/team_play.git

## Commands

To pull all the repositories:

    ./workspace pull

To build:

    ./workspace build
